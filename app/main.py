import requests
import sqlite3
from sqlite3 import Error
import asyncio
from aiohttp import ClientSession
import time
try:
    con = sqlite3.connect('app/test.db')
except Error as e:
        print(e) 

cursorObj = con.cursor()

headers = {
    'Accept': 'application/json',
    'X-Okapi-Key': 'UCeUrQkUi9TBg5ZVlIFycxE0KmtAQMYaZ93n7emCTY/mmODCSc9m4ysy7h7ZwUzf',
}

params = (
    ('lang', 'fr_FR'),
)

def Update_history(History_status,id,con):
    sql_create_tasks_table = """CREATE TABLE IF NOT EXISTS history (
                                    id integer PRIMARY KEY,
                                    status text NOT NULL,
                                    step integer NOT NULL
                                );"""

    
    cursorObj.execute(sql_create_tasks_table)


    for i in range(len(History_status)):
        cursorObj.execute('''INSERT INTO history (status, step) VALUES(?, ?)''', (History_status[i],i))
    print("History table")
    sql_fetch_all_history(con)


def Update_one_letter(id,con):
    response_1_letter = requests.get('https://api.laposte.fr/suivi/v2/idships/'+id, headers=headers, params=params)
    if response_1_letter:
        res = response_1_letter.json()
        ## Status
        Status = [status['shortLabel'] for status in res["shipment"]['timeline'] if status['status'] == True]
        ### add to database
        ### last True Status is the one we need
        print("Before update")
        ids = sql_fetch_all_letters(con)
        if id in ids :
            sql = ''' UPDATE letter
                    SET status = ? 
                    WHERE tracking_number = ?'''
            cursorObj.execute(sql, (Status[len(Status)-1],id))
            con.commit()
        else:
            cursorObj.execute('''INSERT INTO letter (tracking_number, status) VALUES(?, ?)''', (id,Status[len(Status)-1]))
        
        print("After update")
        _ = sql_fetch_all_letters(con)


        return Status
    else:
        print(response_1_letter)
        return None


def Update_one_letter_async(res,con,id):
    ## Status
    if res:
        Status = [status['shortLabel'] for status in res["shipment"]['timeline'] if status['status'] == True]
        print(Status)
        ### add to database
        ### last True Status is the one we need
        print("Before update async")
        ids = sql_fetch_all_letters(con)
        if id in ids :
            sql = ''' UPDATE letter
                    SET status = ? 
                    WHERE tracking_number = ?'''
            cursorObj.execute(sql, (Status[len(Status)-1],id))
            con.commit()
        else:
            cursorObj.execute('''INSERT INTO letter (tracking_number, status) VALUES(?, ?)''', (id,Status[len(Status)-1]))
        
        print("After update async")
        _ = sql_fetch_all_letters(con)


        return Status
    else:
        print("Empty letter")
        print(res)

        return None


def sql_fetch_all_letters(con):

    cursorObj = con.cursor()

    cursorObj.execute('SELECT * FROM letter')

    rows = cursorObj.fetchall()
    ids = []
    for row in rows:
        print(row)
        ids.append(row[1])
    return list(set(ids))

def sql_fetch_all_history(con):

    cursorObj = con.cursor()

    cursorObj.execute('SELECT * FROM history')

    rows = cursorObj.fetchall()
    ids = []
    for row in rows:
        print(row)
        ids.append(row[1])
    return list(set(ids))





async def download_letter_async(id): # A function defined by async def is a native coprocess object
    response =  requests.get('https://api.laposte.fr/suivi/v2/idships/'+id, headers=headers, params=params)
    return response.json()


async def wait_download_async(id):
    res = await download_letter_async(id) # Here download(url) is a native collaboration object
    print(res)
    return Update_one_letter_async(res,con,id)

async def run_prog_async():
    ### Fetch list of letters async    
    List_of_Ids = ['1A00915820380','4P36275770836','1K36275770836','4G11111111110','RK633119313NZ','RK633119313NZ']
    start = time.time()
    await asyncio.wait([wait_download_async(i) for i in List_of_Ids])
    end = time.time()
    print("Complete in {} seconds".format(end - start))

    

if __name__ == "__main__":

    History_status = Update_one_letter("1K36275770836",con)
    Update_history(History_status,"1K36275770836",con)
    print("History of this letter status : ",History_status)


    
    ###Run async
    print("Starty async Job :")
    loop = asyncio.get_event_loop()
    loop.run_until_complete(run_prog_async())

    print("End Async Job : ")
    print("All letters :")
    sql_fetch_all_letters(con)



  